# Zip hierarchy GUI

A GUI pyside app to zip files and folders while keeping their folder structures from a root directory.

Drag'n'drop files/folder into the list window

Set `root` and `zip destination` then click `Run`

## Options

  - Recursive
  - Compress 

## Filters

  - exclude all file matching a pattern
  - include only file matching a pattern
  - exclude git files (*.git, .gitignore)
  - exclude python cache file (*.pyc)
