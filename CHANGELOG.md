# Changelog

0.3.1

- fix: error with directory inclusion filter (stopped at first non-inclusive directory)
- changed: exclude gits is checked by default

0.3.0

- changed: completely refactored the file filter system
    - now use scandir instead of glob (way faster)
    - have separated file/folder include and exclude filters
    - choose if filters are case sensitive

0.2.0

- changed: root tail folder is not included
- changed: sort listed files in folder (just for prints in console)