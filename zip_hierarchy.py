#! bin/python3
## Zip hierarchy GUI v0.3.1

## Python script with PySide2 GUI to Zip file or folder from a root position

## ready to bundle with using pyinstaller with following command:
## pyinstaller.exe --onefile --icon=app.ico zip_hierarchy.py 


# MIT License
# 
# Copyright (c) 2022 Samuel Bernou
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#---------------

import sys
import os, re
import subprocess
import fnmatch
import zipfile as zp

from time import time
from pathlib import Path

#### --- / module auto install to launch as standalone

def module_can_be_imported(name):
    try:
        __import__(name)
        return True
    except ModuleNotFoundError:
        return False

def install_package(name):
    subprocess.run(['python', "-m", "pip", "install", name])


def setup(dependencies):
    '''
    Get a set containing multiple tuple of (module, package) names pair (str)
    install pip with ensurepip if needed, try to import, install, retry
    '''

    # if not module_can_be_imported("pip"):
    #     subprocess.run(['python', "-m", "ensurepip"])

    for module_name, package_name in dependencies:
        if not module_can_be_imported(module_name):
            print(f"Installing module: {module_name} - package: {package_name}")
            install_package(package_name)
            module_can_be_imported(package_name)


# name (used for import), package_name (used for pip install)
dependencies = [
    ('PySide2','PySide2'),
]

if getattr(sys, 'frozen', False) and hasattr(sys, '_MEIPASS'):
    pass # print('running in a PyInstaller bundle')
else:
    setup(dependencies) # bundled, no need for dependancies

#### --- module auto install /

from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtCore import QCoreApplication

## ------
#  CODE
## ------

## -- Filters and Utils funcs

image_exts = ('.png', '.jpg', '.tiff', '.tga', '.jpeg',)
exception_list = ['thumbs.db',]

## not used
# def expand_list(entry_list, recursive=True):
#     '''Get a list of Path objects, mixed files and dirs
#     return a list with directory expanded as files
#     '''

#     pattern = '**/*' if recursive else '*'
#     only_files = []
#     for fp in entry_list:
#         if fp.is_file():
#             only_files.append(fp)
#         else:
#             only_files += sorted([f for f in fp.glob(pattern) if f.is_file()])
#             ## note: glob is the slowest to explore (os.scandir the fastest)

#     return list(set(only_files))

# not used
def sizeof_fmt(num, suffix='B'):
    '''
    return human readable size from bytes amount
    ex: sizeof_fmt(os.stat(filename).st_size)
    '''
    for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)


def filtered_scandir(fp, ex_f=None, in_f=None, ex_d=None, in_d=None, depth=-1, fl=None, sub=0):
    '''
    fp (str): path to explore
    ex_f / in_f / ex_d / in_d (compiled regex): include/exlcude filters are compiled regex
    depth (int): subdirectories depth, 0 = not recursive, -1 = Full recursive
    '''
    if fl is None:
        fl = []
    for item in os.scandir(fp):
        if item.is_dir():
            ## Evaluate inclusion while iterating prevent to check subdirectory inclusion
            ## Check in file level
            # if in_d and not in_d.match(item.name):
            #     continue
            if ex_d and ex_d.match(item.name):
                continue

            if depth:
                filtered_scandir(item.path, ex_f=ex_f, in_f=in_f, ex_d=ex_d, in_d=in_d, depth=depth-1, fl=fl, sub=sub+1)
        else:
            if in_f and not in_f.match(item.name):
                continue
            if ex_f and ex_f.match(item.name):
                continue
            if sub:
                print(item.path,  Path(item.path).parent.parts[-sub:])
            
            ## Dir inclusive : check each parent file parts against include filter
            if sub and in_d and not any(in_d.match(p) for p in Path(item.path).parent.parts[-sub:]):
                continue
            fl.append(item.path)

    return fl


def filtered_explore(fp, in_f=[], ex_f=[], in_d=[], ex_d=[], case_sensitive=False, depth=-1):
    '''Explore given filepath
    exclude/include (list): lists of fnmatch string, ex: ['*.txt', 'h[eo]y', 'version_??'])
    fnmatch patterns:
        * matches everything
        ? matches any single character
        [seq] matches any character in seq
        [!seq] matches any character not in seq
        (to litterally match a character wrap in brackets [])
    depth (int): subdirectories depth, 0 = not recursive, -1 = Full recursive
    case_sensitive (bool): Choose if include/exclude filters are case sensitive (False by default)
    
    return file list matching exclusion and inclusion lists 
    '''

    ## Set include/exclude regex filter - replace names by regex translation
    # Files
    if in_f:
        in_f = r'|'.join([fnmatch.translate(x) for x in in_f])
        in_f = re.compile(in_f) if case_sensitive else re.compile(in_f, flags=re.I)
    if ex_f:
        ex_f = r'|'.join([fnmatch.translate(x) for x in ex_f]) or r'$.'
        ex_f = re.compile(ex_f) if case_sensitive else re.compile(ex_f, flags=re.I)

    # Folder
    if in_d:
        in_d = r'|'.join([fnmatch.translate(x) for x in in_d])
        in_d = re.compile(in_d) if case_sensitive else re.compile(in_d, flags=re.I)
    if ex_d:
        ex_d = r'|'.join([fnmatch.translate(x) for x in ex_d]) or r'$.'
        ex_d = re.compile(ex_d) if case_sensitive else re.compile(ex_d, flags=re.I)

    file_list = filtered_scandir(fp, ex_f=ex_f, in_f=in_f, ex_d=ex_d, in_d=in_d, depth=depth)

    return file_list


### -- Zip func --

def zip_with_structure(path_list, zip, root=None, compressed=True, recursive=True, file_in=None, file_ex=None, dir_in=None, dir_ex=None, case_sensitive=False, no_git=True, no_cache=True, verbose=False):
    '''
    Zip passed path_list into a zip with root path as toplevel tree
    If root is not passed, the shortest path in path_list becomes the root

    :path_list: list list of filepaht as string or Path object (converted anyway)
    :zip: string output fullpath of the created zip
    :root: string top level of the created hierarchy (head not included).
        file that are not under root are discarded
        if not specified, automatically found common highest hierarchy folder
    :compressed: bool Decide if zip is compressed or not
    :recursive: int When a directory is contained in path_list, recursive mode take all sub-directorys files as well
    :file_in: string (coma separated values) Get only files that match this list filter ex: '*.py, *.png'
    :file_ex: string (coma separated values) Get only files that match this list filter ex: '*.pdf, _old'
    :no_git: bool add git files exclusions to exclude_filter ['.git', '.gitignore']
    :no_cache:  bool add python caches exclusions to exclude_filter ['*.pyc', '__pycache__']
    :verbose: int  prints additions in console (faster without verbose at 0)
    '''
    
    # TODO use int values in interface
    depth = -1 if recursive else 0 

    if not zip:
        print('Need zip destination')
        return
    if not Path(zip).parent.exists():
        print('Zip destination parent folder must exists')
        return

    path_list = [Path(f) for f in path_list] # ensure pathlib
    if not path_list:
        print('No file in list')
        return

    if not root:
        # autodetect the path thats is closest to root
        root = sorted(path_list, key=lambda f: f.as_posix().count('/'))[0].parent
        print('Auto determined root:', root)
    else:
        root = Path(root)

    # convert comma separated filters to list 
    file_in = [i.strip() for i in file_in.split(',')] if file_in else []
    file_ex = [i.strip() for i in file_ex.split(',')] if file_ex else []
    dir_in = [i.strip() for i in dir_in.split(',')] if dir_in else []
    dir_ex = [i.strip() for i in dir_ex.split(',')] if dir_ex else []

    print('file_in: ', file_in)
    # Add presets (TODO: replace by prefilled value in interface)
    if no_git:
        file_ex += ['.gitignore']
        dir_ex += ['.git']
    if no_cache:
        file_ex += ['*.pyc']
        dir_ex += ['__pycache__']

    file_ex = list(set(file_ex))
    dir_ex = list(set(dir_ex))

    only_files = []
    for fp in path_list:
        if fp.is_file():
            only_files.append(fp) # direct items are not filtered (seem logical though since user defined...)
        else:
            only_files += filtered_explore(fp, ex_f=file_ex, in_f=file_in, in_d=dir_in, ex_d=dir_ex, case_sensitive=case_sensitive, depth=depth)    
    
    if not only_files:
        print('No file found with current filters')
        return

    only_files = list(set(only_files))

    print(f'Zipping to: {zip}\nWith root {root}')

    t0 = time()
    compress_type = zp.ZIP_DEFLATED if compressed else zp.ZIP_STORED
    with zp.ZipFile(zip, 'w',compress_type) as zipObj:        
        for fp in only_files:
            fp = Path(fp)
            
            if not fp.exists():
                print(f'Not exists: {fp.name}')
                continue

            if str(root) not in str(fp):
                print(f'{fp} is out of root {root}')
                continue

            arcname = fp.as_posix().replace(root.as_posix(), '').lstrip('/')
            if verbose:
                print(f'adding: {arcname}')

            zipObj.write(str(fp), arcname)

    print(f'zipped {len(only_files)} files in {time() - t0:.2f}s')
    return zip


## ------
#  UI
## ------

class ListWidget(QWidget) :
    def __init__(self, label = ''):
        super().__init__()
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)

        self.setMinimumWidth(250)

        # Label
        self.label_layout = QHBoxLayout()
        self.label = QLabel(label)

        # Add
        self.btn_add_src = QPushButton('Add')
        self.btn_add_src.clicked.connect(self.browse)
        self.btn_add_src.setObjectName("btn")

        #DeepSkyBlue, DodgerBlue, Snow, Turquoise, Tomato 
        self.setStyleSheet("""
        QPushButton#btn {
            color: black;
            background-color: LightSlateGray ;
            selection-color: LightCyan;
            selection-background-color: yellow;
        }""")

        # remove
        self.btn_remove_src = QPushButton('Remove')
        self.btn_remove_src.setObjectName("btn")
        self.btn_remove_src.clicked.connect(self.del_input)

        self.label_layout.addWidget(self.label)
        self.label_layout.addStretch()
        self.label_layout.addWidget(self.btn_add_src)
        self.label_layout.addWidget(self.btn_remove_src)


        self.layout.addLayout(self.label_layout)


        self.list = QListWidget(self)
        self.list.setSelectionMode(QAbstractItemView.ExtendedSelection)


        self.setAcceptDrops(True)

        self.layout.addWidget(self.list)

        #self.dropped.connect(self.paths_drop)

    def browse(self) :
        dlg = QFileDialog(self)
        dlg.setFileMode(QFileDialog.Directory)

        if dlg.exec_():
            filenames = dlg.selectedFiles()
            for i in filenames :
                self.add_input(i)


    def dragEnterEvent(self, event) :
        event.accept()

    def dropEvent(self, event):
        # All
        files = ([u.toLocalFile()  for u in event.mimeData().urls()])

        # Folder
        # files = ([u.toLocalFile()  for u in event.mimeData().urls() if isdir( u.toLocalFile() )])

        #files = [unicode(u.toLocalFile()) for u in event.mimeData().urls()]
        for f in files:
            self.add_input(f)

    def add_input(self, text) :
        item = QListWidgetItem(text, self.list)
        item.setFlags(item.flags() | Qt.ItemIsUserCheckable)
        # item.setCheckState(Qt.Unchecked)# unchecked on add
        item.setCheckState(Qt.Checked)# checked on add
        # print('Add Input')

    def del_input(self):
        # deleted = self.list.takeItem(self.list.currentRow())

        rows = [i for i in range(0,self.list.count()) if self.list.item(i).isSelected()]
        print(rows)
        for i in rows[::-1]:
            deleted = self.list.takeItem(i)
            print(deleted.text())

        print('delete')


    def paths_drop(self, paths) :
        print('drop')

class SrcLayout(QHBoxLayout) :
    def __init__(self, layout):
        super().__init__()

        ## Paths list 
        self.path_layout = QVBoxLayout()
        self.wlist = ListWidget('Sources')
        self.path_layout.addWidget(self.wlist)
        self.addLayout(self.path_layout)

        self.settings = QSettings()

        ## Settings
        self.settings_layout = QVBoxLayout()

        # self.settings_layout.addItem(QSpacerItem(0, 35))
        ## content margin order : ←↑→↓
        self.settings_layout.setContentsMargins(5,37,5,7)
        self.settings_layout.setSpacing(20)

        # # self.settings_layout.addWidget( QLabel('Expand instead of compress') )
        # self.cbox_dryrun = QCheckBox('Dry run')
        # self.settings_layout.addWidget(self.cbox_dryrun)
        
        self.cbox_recursive = QCheckBox('Recursive (zip subfolder)')
        self.cbox_recursive.setCheckState(Qt.Checked)
        # self.label = QLabel('Affect subfolder')
        # self.settings_layout.addWidget(self.label)
        self.settings_layout.addWidget(self.cbox_recursive)
        

        ## --- root
        self.label = QLabel('zip root (if not defined, root is auto-set to highest common folder)')
        self.settings_layout.addWidget(self.label)

        self.root_layout = QHBoxLayout()
        self.ebox_root = QLineEdit()
        self.ebox_root.maximumWidth()
        self.root_layout.addWidget(self.ebox_root)
        self.settings_layout.addLayout(self.root_layout)
        
        self.ebox_root.setText(self.settings.value('root')) # remember last runned settings
        

        ## --- zip out
        self.zipout_layout = QHBoxLayout()
        self.label = QLabel('zip destination')
        self.zipout_layout.addWidget(self.label)
        self.ebox_zipout = QLineEdit()
        self.ebox_zipout.maximumWidth()
        self.zipout_layout.addWidget(self.ebox_zipout)
        self.settings_layout.addLayout(self.zipout_layout)

        self.label = QLabel('Filters:')
        self.settings_layout.addWidget(self.label)
        
        self.ebox_zipout.setText(self.settings.value('dest')) # remember last runned settings

        ## --- exclusion file list
        self.file_exclude_layout = QHBoxLayout()
        self.label = QLabel('File exclusion list:')
        self.file_exclude_layout.addWidget(self.label)
        self.ebox_file_exclusions = QLineEdit()
        self.ebox_file_exclusions.maximumWidth()
        # self.ebox_file_exclusions.setTextMargins(5,5,5,5)
        self.ebox_file_exclusions.setPlaceholderText('*.ext, *.')
        self.file_exclude_layout.addWidget(self.ebox_file_exclusions)
        # self.file_exclude_layout.setContentsMargins(5,5,5,5)
        # self.file_exclude_layout.addItem(QSpacerItem(0, 35, hPolicy = QSizePolicy.Preferred)) # Expand ! want to shrink
        self.settings_layout.addLayout(self.file_exclude_layout)
        
        ## --- inclusion file list
        self.file_include_layout = QHBoxLayout()
        self.label = QLabel('File inclusion list:')
        self.file_include_layout.addWidget(self.label)
        self.ebox_file_inclusions = QLineEdit()
        self.ebox_file_inclusions.maximumWidth()
        self.ebox_file_inclusions.setPlaceholderText('*.py, *.pdf')
        self.file_include_layout.addWidget(self.ebox_file_inclusions)
        self.settings_layout.addLayout(self.file_include_layout)

        ## --- exclusion folder list
        self.dir_exclude_layout = QHBoxLayout()
        self.label = QLabel('Folder exclusion list:')
        self.dir_exclude_layout.addWidget(self.label)
        self.ebox_dir_exclusions = QLineEdit()
        self.ebox_dir_exclusions.maximumWidth()
        self.ebox_dir_exclusions.setPlaceholderText('h[eao]y, old_*')
        self.dir_exclude_layout.addWidget(self.ebox_dir_exclusions)
        self.settings_layout.addLayout(self.dir_exclude_layout)
        
        ## --- inclusion folder list
        self.dir_include_layout = QHBoxLayout()
        self.label = QLabel('Folder inclusion list:')
        self.dir_include_layout.addWidget(self.label)
        self.ebox_dir_inclusions = QLineEdit()
        self.ebox_dir_inclusions.maximumWidth()
        self.ebox_dir_inclusions.setPlaceholderText('proj??, render')
        self.dir_include_layout.addWidget(self.ebox_dir_inclusions)
        self.settings_layout.addLayout(self.dir_include_layout)

        self.cbox_case_sensitive = QCheckBox('Case sensitive filters')
        self.settings_layout.addWidget(self.cbox_case_sensitive)

        ## exclusions presets
        self.cbox_ex_gits = QCheckBox('exclude gits')
        self.cbox_ex_gits.setCheckState(Qt.Checked)
        self.settings_layout.addWidget(self.cbox_ex_gits)
        
        self.cbox_ex_cache = QCheckBox('exclude cache')
        self.cbox_ex_cache.setCheckState(Qt.Checked)
        self.settings_layout.addWidget(self.cbox_ex_cache)

        ## Compress
        self.cbox_compressed = QCheckBox('Compress')
        self.cbox_compressed.setCheckState(Qt.Checked)
        self.settings_layout.addWidget(self.cbox_compressed)

        spacer = QSpacerItem(160, 0, hPolicy = QSizePolicy.Preferred)
        self.settings_layout.addItem(spacer)

        self.settings_layout.addStretch()


        # -> maybe add a QVbox layout and use (QSizePolicy.Preferred, QSizePolicy.Expanding) or just (vPolicy = QSizePolicy.Expanding)
        ## Run
        self.btn_run = QPushButton('Run')
        # self.btn_run.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)# expand vertically (if there is a stretch)

        ### self.btn_run.setObjectName("btn")
        self.settings_layout.addWidget(self.btn_run)
        self.btn_run.clicked.connect(self.run)


        self.addLayout(self.settings_layout)
        
        layout.addLayout(self)

    def run(self):
        # items > 'background', 'backgroundColor', 'checkState', 'clone', 'data', 'flags', 'font', 'foreground', 'icon', 
        # 'isHidden', 'isSelected', 'listWidget', 'read', 'setBackground', 'setBackgroundColor', 'setCheckState', 
        # 'setData', 'setFlags', 'setFont', 'setForeground', 'setHidden', 'setIcon', 'setSelected', 'setSizeHint', 
        # 'setStatusTip', 'setText', 'setTextAlignment', 'setTextColor', 'setToolTip', 'setWhatsThis', 'sizeHint', 
        # 'statusTip', 'text', 'textAlignment', 'textColor', 'toolTip', 'type', 'whatsThis', 'write'

        # i.checkState() evaluate true/false (use bool(i.checkState()) to get real boolean value)
        # items = [self.wlist.list.item(i) for i in range(self.wlist.list.count()) if self.wlist.list.item(i).checkState()]# isSelected()
        # for i in items:
        #     print(f'entry: {i.text()}')

        # remember settings for root and dest
        QSettings().setValue('root', self.ebox_root.text())
        QSettings().setValue('dest', self.ebox_zipout.text())

        items = [self.wlist.list.item(i).text() for i in range(self.wlist.list.count()) if self.wlist.list.item(i).checkState()]# isSelected()
        
        txt_recursive = ' recursive' if self.cbox_recursive.isChecked() else ''
        txt_compress = ' compress' if self.cbox_recursive.isChecked() else ''
        print(f'Zip{txt_compress}{txt_recursive}')
        
        zip_with_structure(
            path_list=items,
            zip=self.ebox_zipout.text(),
            root=self.ebox_root.text(),
            compressed=self.cbox_compressed.isChecked(),
            recursive=self.cbox_recursive.isChecked(),
            file_in=self.ebox_file_inclusions.text(),
            file_ex=self.ebox_file_exclusions.text(),
            dir_in=self.ebox_dir_inclusions.text(),
            dir_ex=self.ebox_dir_exclusions.text(),
            case_sensitive=self.cbox_case_sensitive.isChecked(),
            no_git=self.cbox_ex_gits.isChecked(), #False,
            no_cache=self.cbox_ex_cache.isChecked(), #True,
            verbose=1)
        
        # self.cbox_dryrun.isChecked()

class MainWindow(QWidget) :
    def __init__(self):
        super().__init__()
        # QCoreApplication.setOrganizationName('settings')
        # QCoreApplication.setApplicationName('Sequence redundancy handler')
        self.settings = QSettings()
        self.setWindowTitle('Zip Hierarchy') # redundancy handler
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)

        self.src_layout = SrcLayout(self.layout)


### how to store settings

# self.settings = QSettings()
# QSettings() sans arguments ça te retourne les settings de ton application
# QSettings().setValue('pourcentage', 0.5 ) store settings 
# 
# value = QSettings().value('pourcentage')
# self.widget_pourcentage.setValue(value)

if __name__ == '__main__':
    app = QApplication(sys.argv)

    window = MainWindow()
    window.resize(1280, 720)

    window.show()
    app.exec_()

